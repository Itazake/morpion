#include <iostream>
#include <vector>
#include <string>
#include <stdexcept>

using namespace std;

enum Frame { Empty, Cross, Circle };
Frame tab[3][3] = { {Empty, Empty, Empty}, {Empty,Empty,Empty}, {Empty,Empty,Empty} };

// Display X, O or .
const void printFrame(int i, int j) {
   switch(tab[i][j]) {
       case Empty :
           cout << '.';
           break;
       case Cross :
           cout << 'X';
           break;
       case Circle :
           cout << 'O';
           break;
   }
}

// Print the whole Board Game
const void printBoard() {
   for (int i = 0 ; i<3 ; i++) {
       for (int j = 0 ; j<3 ; j++) {
           printFrame(i, j);
       }
       cout << endl;
   }
}

// Play
// convert frameNum to [x][y];
// put frame in tab[x][y]
void PlayInBoard(int frameNum, Frame value) {
    int x = frameNum % 3;
    int y = frameNum-(3*x);
    tab[x][y] = value;
}

/* return who win
 *
 * Empty  = Nobody
 * Cross  = Cross 
 * Circle = Circle
 */
Frame CheckWhoWon() {
    // il faut vérifier toutes les conditions pour voir qui a gagné, 
    // diagonales
    if(tab[0][0] == tab[1][1] && tab[0][0] == tab[2][2] )
        return tab[0][0];
    if(tab[2][0] == tab[1][1] && tab[2][0] == tab[0][2] )
        return tab[2][0];
    // horizontal
    if(tab[0][0] == tab[0][1] && tab[0][0] == tab[0][2] )
        return tab[0][0];
    if(tab[1][0] == tab[1][1] && tab[1][0] == tab[1][2] )
        return tab[1][0];
    if(tab[2][0] == tab[2][1] && tab[2][0] == tab[2][2] )
        return tab[2][0];
    // vertical
    if(tab[0][0] == tab[1][0] && tab[0][0] == tab[2][0] )
        return tab[0][0];
    if(tab[0][1] == tab[1][1] && tab[0][1] == tab[2][1] )
        return tab[1][0];
    if(tab[0][2] == tab[1][2] && tab[0][2] == tab[2][2] )
        return tab[0][2];
    // on retourne Empty, Cross ou Circle en fonction
    return Empty;
}

bool tryParse(string& input, int& output) {
    try{
        output = stoi(input);
    } catch (invalid_argument) {
        return false;
    }
    return true;
}

int main(int main (int argc, char *argv[])) {

    cout << "Welcome to Shi-Fu-Mi Game !" << endl;
    Frame currentFrame = Cross;
    int nbMove = 0;
    int frameNum = -1;
    bool quit = false;
    Frame winner = Empty;
    vector<int> previous; 
    // better than cin
    string input;
    int x;


    while (nbMove < 9 && winner == Empty) {

        // [BETTER] recuperer le numero de la case ou on souhaite jouer
        getline(cin, input);
        while (!tryParse(input, frameNum))
        {
            cout << "Veuillez saisir un NOMBRE : ";
            getline(cin, input);
        }
        frameNum--;
        // verification numero choisi
        if(frameNum < 0 || frameNum > 8){
            cout << "Veuillez jouer entre 1 et 9 : ";
            continue;
        }
        // verification numero pas deja joué
        for (auto i = previous.begin(); i != previous.end(); ++i) 
        {
            if( *i == frameNum ) {
                quit = true;
                continue;
            }
        }
        if(quit) {
            cout << "Veuillez jouer dans une case vide : ";
            quit = false;
            continue;
        }
        previous.push_back(frameNum);// memorise le coup joué
        PlayInBoard(frameNum, currentFrame);// mettre un “currentFrame” dans la case sélectionnée
        
        printBoard();// affiche le plateau de jeu
        cout << endl;

        // check si il y a une victoire
        winner = CheckWhoWon();
        if( winner != Empty)
        {
            cout << "Victoire de " << winner;
            continue;
        }
        nbMove++;
        if( currentFrame == Cross ) {
            currentFrame = Circle;
        } else {
            currentFrame = Cross;
        }
    }
    if (nbMove == 9 && winner == Empty) {
        cout << "Egalite !" << endl;
    }
  
   //getchar();
   return 0;
}
